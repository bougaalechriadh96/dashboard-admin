const Content = ({ data, users, token, loading, setLoading }) => {
  return (
    <div className="container-table">
      <table>
        <thead>
          <tr>
            <th>#</th>
            <th style={{ width: "300px" }}>name</th>
            <th>email</th>
            <th>Logged With : </th>
          </tr>
        </thead>
        <tbody>
          {data.map((user, i) => (
            <tr key={user._id}>
              <td>{i + 1}</td>
              <td>
                <div style={{ position: "relative" }}>
                  <img
                    src={`http://34.121.25.60:5000/uploads/${user.avatar}`}
                    className="img-circle"
                    alt="logo"
                    style={{
                      width: "40px",
                      height: "40px",
                      borderRadius: "50%",
                      position: "absolute",
                      top: "-20px",
                      left: "-30px",
                    }}
                  />
                  <p
                    style={{ position: "absolute", top: "-10px", left: "20px" }}
                  >
                    {user.fullname}
                  </p>
                </div>
              </td>
              <td>{user.email}</td>
              <td>
                {user.isLoggedInWithGmail == 1 ? (
                  <div>
                    <div>
                      <i className="fab fa-google"></i>
                    </div>
                  </div>
                ) : (
                  <></>
                )}
                {user.isLoggedInWithIOS == 1 ? (
                  <div>
                    <div>
                      <i className="fab fa-apple"></i>
                    </div>
                  </div>
                ) : (
                  <></>
                )}
                {user.isLoggedInWithEmail == 1 ? (
                  <div>
                    <div>
                      <i className="fa fa-envelope" aria-hidden="true"></i>
                    </div>
                  </div>
                ) : (
                  <></>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Content;
