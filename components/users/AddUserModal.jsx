import { useState, useEffect } from "react";
import axios from "axios";
import { API_URL, EMAIL_FORMAT, USER_ROLES } from "../../utils/consts";
import { markInputAsFalse, markInputAsTrue } from "../../utils/inputHandlers";
import { Modal, MultiSelect } from "../widgets";

const AddUserModal = ({ token, isModalOpen, getAllUsers, handleClose }) => {
	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	
	
	const handleAddUser = () => {
		if (isValid()) {
			const config = {
				method: "POST",
				url: `http://141.95.149.78:5001/api/v1/users`,
				headers: {
					Authorization: `Bearer ${token}`,
				},
				data: { name, email, password},
			};

			axios(config)
				.then(({ status }) => {
					if (status === 200) {
						getAllUsers();
						clearAndCloseModal();
					}
				})
				.catch((err) => {
					if (err.response) {
						const { status, data } = err.response;

						if (status === 400 || status === 401) {
							markInputAsFalse("form", data.error);
						}
					}
				});
		}
	};

	const handleInputChange = (e, fn) => {
		fn(e.target.value);
		markInputAsTrue(e.target.id);
	};

	const isValid = () => {
		if (!name) {
			markInputAsFalse("user-name", "Name is required");
			return false;
		}
		if (!email) {
			markInputAsFalse("user-email", "Email is required");
			return false;
		}
		if (!email.match(EMAIL_FORMAT)) {
			markInputAsFalse("user-email", "Email is not valid");
			return false;
		}
		if (!password) {
			markInputAsFalse("user-password", "Password is required");
			return false;
		}
		if (password.length < 6) {
			markInputAsFalse("user-password", "Password is shorter than the minimum allowed length (6)");
			return false;
		}
		

		return true;
	};

	const clearAndCloseModal = () => {
		setName("");
		setEmail("");
		setPassword("");
	
		

		markInputAsTrue("user-name");
		markInputAsTrue("user-email");
		markInputAsTrue("user-password");
		markInputAsTrue("form");
		handleClose();
	};

	return (
		<>
			<Modal
				title='Add user'
				isModalOpen={isModalOpen}
				handleClose={clearAndCloseModal}
				footer={<button onClick={handleAddUser}>Add User</button>}>
				<form id='form'>
					<input
						id='user-name'
						type='text'
						placeholder='Name'
						value={name}
						onChange={(e) => handleInputChange(e, setName)}
					/>
					<input
						id='user-email'
						type='email'
						placeholder='Email'
						autoComplete='username'
						value={email}
						onChange={(e) => handleInputChange(e, setEmail)}
					/>
					<input
						id='user-password'
						type='password'
						placeholder='Password'
						autoComplete='current-password'
						value={password}
						onChange={(e) => handleInputChange(e, setPassword)}
					/>
					
			

				</form>
			</Modal>

			<style jsx>{`
				input,
				select {
					margin-bottom: 10px;
				}
			`}</style>
		</>
	);
};

export default AddUserModal;
