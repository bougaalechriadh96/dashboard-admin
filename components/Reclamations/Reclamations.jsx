import { useState, useEffect } from "react";
import axios from "axios";
import { markInputAsFalse, markInputAsTrue } from "../../utils/inputHandlers";
import { Modal } from "../widgets";
import { API_URL } from "../../utils/consts";
import { UPLOAD_URL } from "../../utils/consts";
const Reclamations = ({ token, isModalOpen, handleClose, reclamationId }) => {
  const [reclamation, setReclamation] = useState([]);
  const [user, setUser] = useState([]);
  const [book, setBook] = useState([]);

  const getReclamation = () => {
    const config = {
      method: "GET",
      url: `${API_URL}/admins/bookreclamations/${reclamationId}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    axios(config)
      .then(({ status, data }) => {
        if (status === 200) {
          setReclamation(data.data);
          setUser(data.data.user);
          setBook(data.data.book);
        }
      })
      .catch((err) => {
        console.log("hello");
        console.error("err", err);
      });
  };
  useEffect(() => {
    getReclamation();
  }, [reclamationId]);

  const isValid = () => {
    if (!title) {
      markInputAsFalse(" reclamation-title", "title is required ");
      return false;
    }

    return true;
  };

  const clearAndCloseModal = () => {
    handleClose();
  };

  return (
    <>
      <Modal isModalOpen={isModalOpen} handleClose={clearAndCloseModal}>
        <div className="container">
          <div className="user">
            <img
              className="profile"
              src={`${UPLOAD_URL}/${user && user.avatar}`}
            />
            <span className="name">{user && user.fullname}</span>
          </div>
          <div className="message">
            <span>Message :</span>
            <br />

            <p>{reclamation && reclamation.message}</p>
          </div>
          <div className="book">
            <img
              className="cover"
              src={`${UPLOAD_URL}/${book && book.cover}`}
            />
          </div>
        </div>
      </Modal>
      <style jsx>{`
        @import url("https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;1,300&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,200&display=swap");
        .container {
          position: relative;
        }
        .message span {
          font-size: 16px;
          color: #455a64;
          font-family: Poppins;
          font-weight: bold;
          margin-left: 10px;
        }
        .message p {
          font-size: 16px;
          color: #455a64;

          margin-left: 7px;
        }
        .profile {
          height: 50px;
          width: 50px;
          border-radius: 50%;
          position: absolute;
          top: 0;
          left: 3px;
          margin-top: 5px;
        }
        .message span {
          margin-top: 10px;
        }
        .name {
          position: absolute;
          top: 20px;
          left: 70px;
          font-weight: bold;
          line-height: 1.2;
          color: #455a64;
          font-size: 18px;
          color: #455a64;
          font-family: Poppins;
        }
        .user {
          width: 250px;
          height: 60px;
          border: 1px solid #dee2e6 !important;
          margin-left: 250px;
          position: relative;
          padding: 30px;
        }
        .cover {
          min-height: 260px;
          width: 240px;
        }
        .message {
          width: 250px;
          min-height: 200px;
          border: 2px solid #dee2e6 !important;
          padding: 10px;
          margin-left: 250px;
          overflow-wrap: break-word;
        }
        .book {
          position: absolute;
          top: 0;
          left: 0;
          width: 240px;
          min-height: 270px;
        }
      `}</style>
    </>
  );
};

export default Reclamations;
