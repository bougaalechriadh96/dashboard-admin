import React, { useEffect, useState } from "react";

const Ebook = ({ setFiltered, ebooks, filtered }) => {
  const [search, setSearch] = useState("");
  const [activeGenre, setActiveGenre] = useState("");

  useEffect(() => {
    if (activeGenre === "all") {
      setFiltered(ebooks);

      return;
    }
    const filtered = ebooks.filter((ebook) =>
      ebook.hiden.hide.toString().includes(activeGenre)
    );
    setFiltered(filtered);
  }, [activeGenre]);
  useEffect(() => {
    if (setSearch !== "") {
      const filtered = ebooks.filter((pbook) =>
        pbook.title.toLowerCase().includes(search.toLowerCase())
      );
      setFiltered(filtered);

      return;
    }
  }, [search]);
  const handleChange = (e) => {
    setSearch(e.target.value);
  };
  const hidden = ebooks.filter((ebook) => {
    return ebook.hiden.hide.toString().includes("true");
  });
  const unhidden = ebooks.filter((ebook) => {
    return ebook.hiden.hide.toString().includes("false");
  });
  return (
    <div>
      <div className="container">
        <button onClick={() => setActiveGenre("true")}>
          {" "}
          hidden ({hidden.length})
        </button>
        <button onClick={() => setActiveGenre("false")}>
          unhidden ({unhidden.length})
        </button>
        <button className="all" onClick={() => setActiveGenre("all")}>
          All ({ebooks.length})
        </button>
      </div>
      <div className="searchbar">
        <input
          className="search"
          type="text"
          value={search}
          onChange={handleChange}
          placeholder="Search..."
        />
      </div>
      <style jsx>{`
        button {
          margin-right: 2rem;
          min-width: 85px;
          padding: 0.5rem 0.5rem;
          border: none;
          height: 30px;
          color: white;
          border-radius: 5px;
          dispaly: flex;
          align-items: center;
          justify-content: center;
          font-size: 13px;
          cursor: pointer;
          font-weight: 400;
        }
        button p {
          margin-bottom: 8px;
        }
        button:active {
          background-color: #eee;
          color: #373a3c;
        }
        button:focus {
          background-color:#1e88e5;
          outline-offset: 1px;
        }

        .container {
          margin-left: 10px;
        }

        input {
          width: 250px;
          margin-left: 1400px;
          height: 30px;
        }
      `}</style>
    </div>
  );
};

export default Ebook;
