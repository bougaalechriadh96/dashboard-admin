import { useState } from "react";
import { ToastContainer } from "react-toastify";
import Header from "./Header";
import Nav from "./Nav";

const Layout = ({ activeSection, children }) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <>
      <Header isMenuOpen={isMenuOpen} setIsMenuOpen={setIsMenuOpen} />

      <main>
        <div className="nav-wrapper">
          <Nav activeSection={activeSection} />
        </div>

        <div className="content-wrapper">{children}</div>

        <ToastContainer />
      </main>
      <style jsx>{`
        main {
          display: flex;
          flex: row;
          align-items: stretch;
          height: 100vh;
        }

        .nav-wrapper {
          width: 240px;
          height: 100%;
          background-color: #242424;
          color: #fff;
          overflow-y: auto;
          padding: 20px 10px;
        }
        .content-wrapper {
          width: calc(100% - 240px);
          padding: 20px 30px 82px;
          background-color: #f2f2f2;
          overflow-y: auto;
        }

        @media only screen and (max-width: 1024px) {
          .nav-wrapper {
            width: 180px;
          }

          .content-wrapper {
            width: calc(100% - 180px);
          }
        }

        @media only screen and (max-width: 1023px) {
          .nav-wrapper {
            position: absolute;
            top: 0;
            width: 280px;
            height: 100%;
            z-index: 998;
            padding-top: 60px;
            transition: left 300ms ease;
            left: ${isMenuOpen ? "0px" : "-330px"};
          }

          .content-wrapper {
            width: 100%;
            padding-top: 60px;
            padding-right: 10px;
            padding-left: 10px;
          }
        }
      `}</style>
    </>
  );
};

export default Layout;
