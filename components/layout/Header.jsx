import Image from "next/image";

const Header = ({ isMenuOpen, setIsMenuOpen }) => {
	return (
		<>
			<div
				className='container-header'
				onClick={() => {
					setIsMenuOpen(!isMenuOpen);
				}}>
				<div className='header'>
					{isMenuOpen ? <i className='fas fa-times'></i> : <i className='fas fa-bars'></i>}

					
				</div>
			</div>

			<style jsx>{`
				.container-header {
					display: none;
					clear: both;
				}

				.logo {
					width: 50px;
					height: 40px;
					position: relative;
				}
    
				@media only screen and (max-width: 1023px) {
					.container-header {
						width: 100%;
						display: block;
						padding: 10px 10px;
						background-color: #242424;
						position: fixed;
						top: 0;
						left: 0;
						z-index: 999;
					}

					.header {
						display: flex;
						align-items: center;
						justify-content: space-between;
					}

					i {
						font-size: 24px;
						color: #e1cb1f;
						cursor: pointer;
					}
				}
			`}</style>
		</>
	);
};

export default Header;
