import { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import cookie from "js-cookie";
import ROUTES from "../../utils/routes";

const Nav = ({ activeSection }) => {
  const router = useRouter();
  const [email, setEmail] = useState("");

  useEffect(() => {
    const _email = cookie.get("email");

    setEmail(_email ? _email : "Unknown");
  }, []);

  const logout = () => {
    cookie.remove("token");
    cookie.remove("role");
    router.push("/");
  };

  return (
    <>
      <nav>
        <div>
          <div className="logo">
            <img src="/img/logo.svg" alt="" style={{ height: "80px" }} />
          </div>
          {ROUTES.map(({ title, link, icon }, i) => (
            <Link key={i} href={`/${link}`}>
              <a
                className={activeSection === i ? "nav-item active" : "nav-item"}
              >
                <i className={icon}></i>
                <p>{title}</p>
              </a>
            </Link>
          ))}
        </div>

        <div>
          <p className="logout" onClick={() => logout()}>
            Log out
          </p>
        </div>
      </nav>

      <style jsx>{`
        nav {
          height: 100%;
          display: flex;
          flex-direction: column;
          justify-content: space-between;
          text-align: center;
        }

        .logo {
          height: 80px;
          margin-bottom: 40px;
          position: relative;
        }

        .nav-item {
          text-align: left;
          padding: 10px 15px;
          margin-bottom: 6px;
          font-size: 14px;
          border-radius: 4px;
          transition: all 300ms ease;
          font-weight: 400;
          color: white;
          text-decoration: none;
          display: flex;
          align-items: center;
          gap: 10px;
        }

        .nav-item:hover,
        .nav-item.active {
          background-color: #72a0c1;
          color: white;
        }

        i {
          font-size: 16px;
        }

        .email {
          font-size: 12px;
          color: #fff;
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
          text-align: left;
          margin-bottom: 4px;
        }

        .logout {
          font-size: 12px;
          color: #fff;
          text-decoration: none;
          border-bottom: 1px solid transparent;
          opacity: 0.72;
          transition: all 0.25s ease-in;
          width: fit-content;
          cursor: pointer;
        }

        .logout:hover {
          border-bottom: 1px solid #fff;
          opacity: 1;
        }

        @media only screen and (max-width: 1023px) {
          .logo {
            display: none;
            clear: both;
          }
        }
      `}</style>
    </>
  );
};

export default Nav;
