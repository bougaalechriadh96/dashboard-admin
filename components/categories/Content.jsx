import Image from "next/image";
import { UPLOAD_URL } from "../../utils/consts";
const Content = ({ data }) => {
  return (
    <div className="container-table">
      <table>
        <thead>
          <tr>
            <th>#</th>
            
            <th>icon</th>
            <th>image</th>
            <th>title</th>
            <th>update</th>
          </tr>
        </thead>
        <tbody>
          {data.map((category, i) => (
            <tr key={category._id}>
              <td>{i + 1}</td>
              
              <td>
                <img
                  src={`${UPLOAD_URL}/${category.icon}`}
                  width="50"
                  height="50"
                />
              </td>
              <td>
                <img
                  src={`${UPLOAD_URL}/${category.image}`}
                  width="50"
                  height="50"
                />
              </td>
              <td>{category.title}</td>
              <td>
                <button onClick={() => onclick(reclamation._id)}>
                      update
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Content;
