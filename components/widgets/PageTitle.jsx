import { useRouter } from "next/router";

const PageTitle = ({ title, withGoBack = false }) => {
	const router = useRouter();

	return (
		<>
			<div className='page__title'>
				{withGoBack && (
					<div className='action-icon back' onClick={() => router.back()}>
						<i className='fas fa-chevron-left'></i>
					</div>
				)}
				<h1>{title}</h1>
			</div>

			<style jsx>{`
				.page__title {
					padding: 24px 0px 17px;
					border-bottom: 1px solid #e0e4e6;
					margin-bottom: 32px;
					display: flex;
					align-items: center;
				}

				.back {
					margin-right: 20px;
					cursor: pointer;
				}

				h1 {
					font-size: 23px;
					line-height: 38px;
					letter-spacing: -0.6px;
				}
			`}</style>
		</>
	);
};

export default PageTitle;
