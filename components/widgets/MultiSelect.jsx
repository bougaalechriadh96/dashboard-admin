import { useEffect, useState } from "react";
import { markInputAsTrue } from "../../utils/inputHandlers";

const MultiSelect = ({ id, defaultValue = "", items, title, value, selectedElements, setSelectedElements }) => {
	const [localValue, setLocalValue] = useState("");
	const [selectedValues, setSelectedValues] = useState([]);

	useEffect(() => {
		const _selectedValues = [];

		for (const selectedElm of selectedElements) {
			const _value = items.find((elm) => elm[value] === selectedElm)[title];
			if (_value) {
				_selectedValues = [..._selectedValues, _value];
			}
		}

		setSelectedValues(_selectedValues);
	}, [selectedElements]);

	const handleChange = (e) => {
		const { value } = e.target;
		setLocalValue(value);

		if (value && !selectedElements.includes(value)) {
			setSelectedElements([...selectedElements, value]);
		}

		markInputAsTrue(id);
	};

	const deleteElemente = (i) => {
		const _selectedElements = [...selectedElements];

		_selectedElements.splice(i, 1);

		setSelectedElements(_selectedElements);
		setLocalValue("");
	};

	return (
		<>
			<select id={id} value={localValue} onChange={handleChange}>
				<option value=''>{defaultValue}</option>
				{items.map((item, i) => (
					<option key={i} value={item[value]}>
						{item[title]}
					</option>
				))}
			</select>

			{Array.isArray(selectedValues) && selectedValues.length > 0 && (
				<div className='selected-selectedValues'>
					{selectedValues.map((elm, i) => (
						<div key={i} className='element' onClick={() => deleteElemente(i)}>
							<p>{elm}</p>
							<i className='fas fa-times'></i>
						</div>
					))}
				</div>
			)}

			<style jsx>{`
				.selected-selectedValues {
					display: flex;
					align-items: center;
					flex-wrap: wrap;
					gap: 5px;
					margin-top: 15px;
				}

				.element {
					padding: 4px 10px;
					background-color: #e1cb1f;
					border-radius: 3px;
					display: flex;
					align-items: center;
					gap: 15px;
					cursor: pointer;
				}

				p {
					font-size: 14px;
				}

				i {
					font-size: 14px;
				}
			`}</style>
		</>
	);
};

export default MultiSelect;
