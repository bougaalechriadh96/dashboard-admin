import BtnAdd from "./BtnAdd";

const PageHeader = ({ total, type, handleAdd, handleEdit }) => {
	return (
		<>
			<div className='page-header'>
				<p>
					Showing {total}{" "}
					{type.charAt(type.length - 1) === "y" ? `${type.slice(0, -1).toLowerCase()}ies` : `${type.toLowerCase()}s`}
				</p>
				{handleAdd && <BtnAdd title={`Add new ${type}`} handleClick={handleAdd} />}
				{handleEdit && <BtnAdd title={`Configure default ${type}`} handleClick={handleEdit} />}
			</div>

			<style jsx>{`
				.page-header {
					display: flex;
					align-items: center;
					justify-content: space-between;
				}
				p {
					flex: 1;
					font-size: 15px;
					color: #4a5155;
				}
			`}</style>
		</>
	);
};

export default PageHeader;
