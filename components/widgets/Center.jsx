import React from "react";

const Center = ({ children }) => {
	return (
		<>
			<div className='center'>{children}</div>

			<style jsx>{`
				.center {
					width: 100%;
					height: 100%;
					display: flex;
					flex-direction: column;
					align-items: center;
					justify-content: center;
				}
			`}</style>
		</>
	);
};

export default Center;
