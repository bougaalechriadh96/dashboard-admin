const BtnAdd = ({ title, handleClick }) => {
	return (
		<>
			<button type='button' className='btn' onClick={() => handleClick()}>
				{title}
			</button>
		</>
	);
};

export default BtnAdd;
