const Actions = ({ handleDelete, handleEdit }) => {
  return (
    <>
      <div className="d-flex">
        {handleDelete && (
          <div className="action-icon delete" onClick={handleDelete}>
            <i className="fas fa-trash" />
          </div>
        )}

        {handleEdit && (
          <div className="action-icon edit" onClick={handleEdit}>
            <i className="fas fa-pencil-alt" />
          </div>
        )}
      </div>
    </>
  );
};

export default Actions;
