import Link from "next/link";

const CardAnalytics = ({ title = "", value = 0, icon, color = "#578ebe", link = "" }) => {
	return (
		<>
			<Link href={link}>
				<a>
					<div className='card'>
						<p className='value'>{value}</p>
						<p className='title'>{title}</p>
						{icon && <i className={icon} />}

						<div className='overlay'></div>
					</div>
				</a>
			</Link>

			<style jsx>{`
				.card {
					padding: 30px 10px 50px;
					border-radius: 3px;
					overflow: hidden;
					position: relative;
					cursor: pointer;
					background-color: ${color};
				}

				.value {
					font-size: 34px;
					line-height: 36px;
					font-weight: 500;
					color: #fff;
				}

				.title {
					font-size: 16px;
					font-weight: 300;
					color: #fff;
					margin-top: 5px;
					text-transform: capitalize;
				}

				i {
					font-size: 100px;
					color: #fff;
					opacity: 0.3;
					position: absolute;
					right: -20px;
					bottom: -10px;
				}

				.overlay {
					width: 100%;
					height: 100%;
					position: absolute;
					top: 0;
					left: 0;
					background-color: #000;
					opacity: 0;
					transition: opacity 300ms ease;
				}

				.card:hover .overlay {
					opacity: 0.2;
				}
			`}</style>
		</>
	);
};

export default CardAnalytics;
