import { useEffect, useState } from "react";
import axios from "axios";
import Layout from "../components/layout";
import { Container, PageTitle, PageHeader } from "../components/widgets";
import { requirePageAuth } from "../utils/auth";
import Content from "../components/users/Content";
import Logged from "../components/filter/Logged";
import AddUserModal from "../components/users/AddUserModal";
import { API_URL } from "../utils/consts";
const Index = ({ token }) => {
  const [users, setUsers] = useState([]);
  const [isAddModalOpen, setIsAddModalOpen] = useState(false);
  const [filtered, setFiltered] = useState([]);
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState("");

  useEffect(() => {
    getAllUsers();
  }, [!loading]);

  const getAllUsers = () => {
    const config = {
      method: "GET",
      url: `${API_URL}/admins/users`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    axios(config)
      .then(({ status, data }) => {
        if (status === 200) {
          setUsers(data.data);
          setFiltered(data.data);
        }
      })
      .catch((err) => {
        console.error("err", err);
      });
  };

  const handleCloseAddModal = () => {
    setIsAddModalOpen(false);
  };

  return (
    <Layout activeSection={0}>
      <PageTitle title="Users" />
      
      <Container>
      <Logged
        users={users}
        setFiltered={setFiltered}
        filtered={filtered}
      ></Logged>
        <Content
          data={filtered}
          token={token}
          users={users}
          setLoading={setLoading}
          loading={loading}
        />
      </Container>
      <AddUserModal
        isModalOpen={isAddModalOpen}
        token={token}
        handleClose={handleCloseAddModal}
        getAllUsers={getAllUsers}
      />
    </Layout>
  );
};

export const getServerSideProps = requirePageAuth;

export default Index;
