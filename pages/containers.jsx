import Content from "../components/containers/Content";
import Layout from "../components/layout";
import { Container, PageTitle } from "../components/widgets";
import { requirePageAuth } from "../utils/auth";
import serverSideGetter from "../utils/serverSideGetter";

const Index = ({ zones }) => {
  return (
    <Layout activeSection={3}>
      <PageTitle title="Containers - All zones" />
      <Container>
        <Content zones={zones} />
      </Container>
    </Layout>
  );
};

export const getServerSideProps = async (ctx) => {
  const { props, redirect } = requirePageAuth(ctx);

  if (props.role !== "admin") {
    return {
      props: {},
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  const data = await serverSideGetter("zones", props.token);

  return {
    props: {
      token: props.token,
      zones: data && data.data ? data.data : [],
    },
    redirect,
  };
};

export default Index;
