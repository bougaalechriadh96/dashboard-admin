import React , {useState , useEffect} from "react";
import { useRouter } from "next/router";
import Layout from "../../components/layout";
import { Container, PageTitle } from "../../components/widgets";
import axios from "axios";
import { requirePageAuth } from "../../utils/auth";

const Pbookinfos = ({ token, data }) => {
  const [book, setBook] = useState([]);

  const router = useRouter();
  const { id } = router.query;

  console.log("id :>> ", id);
  const pbookInfos = () => {
    const config = {
      method: "GET",
      url: `http://141.95.149.78:5001/api/v1/physicalbooks/${id}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    console.log(id);
    axios(config)
      .then(({ status, data }) => {
        setBook(data.data);
      })
      .catch((err) => {
        console.error("err", err);
      });
  };
  useEffect(() => {
    pbookInfos();
  }, []);

  return (
    <Layout activeSection={4}>
      <PageTitle title="" />
      <Container></Container>
    </Layout>
  );
};
export const getServerSideProps = requirePageAuth;
export default Pbookinfos;
