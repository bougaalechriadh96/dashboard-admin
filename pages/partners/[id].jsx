import  {useState , useEffect} from "react";
import { useRouter } from "next/router";
import Layout from "../../components/layout";
import { Container, PageTitle } from "../../components/widgets";
import axios from "axios";
import { requirePageAuth } from "../../utils/auth";
import { API_URL } from "../../utils/consts";

const Partnerinfos = ({ token, data }) => {
  const [user, setUser] = useState([]);
  const [partner, setPartner] = useState([]);
  const [content, setContent] = useState([[]]);
  const router = useRouter();
  const { id } = router.query;

  console.log("id :>> ", id);
  const partnerInfos = () => {
    const config = {
      method: "GET",
      url: `${API_URL}/partners/${id}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    console.log(id);
    axios(config)
      .then(({ status, data }) => {
        setUser(data.data.user);
        setPartner(data.data);
        setContent(data.data);
        console.log(data);
      })
      .catch((err) => {
        console.error("err", err);
      });
  };
  useEffect(() => {
    partnerInfos();
  }, []);

  return (
    <Layout activeSection={1}>
      <div className="container bootstrap snippets bootdey">
        <div className="row">
          <div className="col-md-12">
            <div className="grid profile">
              <div className="grid-header">
                <div className="col-xs-2">
                  <img
                    src={`http://34.121.25.60:5000/uploads/${user.avatar}`}
                    className="img-circle"
                    alt="logo"
                  />
                </div>
                <div className="col-xs-7">
                  <h3>{user.fullname}</h3>

                  <br />
                </div>
                <div className="followers">
                  2K+ <p>folowers</p>
                </div>
                <div className="following">
                  500 <p>following</p>
                </div>
                <div className="friends">
                  300 <p>friends</p>
                </div>
              </div>
              <div className="about">
                <span>Contact</span>
                <div className="email">
                  <div className="icon">
                    <i className="fa fa-envelope fa-lg" aria-hidden="true">
                      <span>email:</span>
                      <br></br>
                      <p>{user.email}</p>
                    </i>
                  </div>
                </div>
                <div className="phone">
                  <div className="icon">
                    <i className="fas fa-phone fa-lg">
                      <span>phone:</span>
                      <br></br>
                      <p>{partner.phone}</p>
                    </i>
                  </div>
                </div>
              </div>
              <div className="contact">
                <span>About</span>
                <div className="company">
                  <div className="icon">
                    <i className="fas fa-building fa-lg" aria-hidden="true">
                      <p>
                        CompanyLink : <br></br>
                        <span>{partner.companyLink}</span>
                      </p>
                    </i>
                  </div>
                </div>
                <div className="location">
                  <div className="icon">
                    <i className="fa fa-map-marker fa-lg">
                      <p>
                        Address : <br></br>
                        <span>{partner.address}</span>{" "}
                      </p>
                    </i>
                  </div>
                </div>
                <div className="description">
                  <div className="icon">
                    <i className="fa fa-info-circle fa-lg" aria-hidden="true">
                      <p>
                        Description :<br></br>{" "}
                        <span>{partner.description}</span>{" "}
                      </p>
                    </i>
                  </div>
                </div>
                <div className="work">
                  <div className="icon">
                    <i className="fa fa-briefcase" fa-lg aria-hidden="true">
                      <p>
                        field of work :<br></br>{" "}
                        <span>{partner.fieldOfWork}</span>{" "}
                      </p>
                    </i>
                  </div>
                </div>
                <div className="status">
                  <div className="icon">
                    <i className="fa fa-check" fa-lg aria-hidden="true">
                      <p>
                        Status :<br></br> <span>{partner.status}</span>{" "}
                      </p>
                    </i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <style jsx>{`
        @import url("https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;1,300&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,200&display=swap");
        .profile .grid-header {
          background-color: #cccccc;
          background-image: url("/img/profile-bg-img.png");
          color: #f5f5f5;
          height: 300px;
          z-index: 20;
        }

        i {
          font-size: 1.25em;
        }
        .phone i span {
          font-size: 13px;
          color: #00000099;
          font-weight: 400;
          font-family: Poppins;
        }
        .email i span {
          font-size: 13px;
          color: #00000099;
          font-weight: 400;
          font-family: Poppins;
        }
        .about {
          position: absolute;
          width: 500px;
          height: 200px;
          background-color: white;
          top: 250px;
          left: 80px;
          z-index: 30;
          border-radius: 5px;
          box-shadow: 0px 1px 3px rgb(0 0 0 / 20%), 0px 2px 1px rgb(0 0 0 / 12%),
            0px 1px 1px rgb(0 0 0 / 14%);
          padding-top: 20px;
          padding-left: 20px;
        }
        .pabout {
          font-weight: bold;
        }
        .pcontact {
          font-weight: bold;
        }

        .contact {
          position: absolute;
          width:900px;
          height: 300px;
          background-color: white;
          top: 250px;
          left: 650px;
          z-index: 30;
          border-radius: 5px;
          box-shadow: 0px 1px 3px rgb(0 0 0 / 20%), 0px 2px 1px rgb(0 0 0 / 12%),
            0px 1px 1px rgb(0 0 0 / 14%);
          padding-top: 20px;
          padding-left: 20px;
        }

        .email {
          height: 50px;
          width: 270px;

          margin-top: 10px;
        }
        .email .icon p {
          margin-top: 90px;
          display: inline;
          margin-left: 60px;
          color: #0000ee;
          font-size: 16px;
          font-weight: bold;
          line-height: 1.43;
        }

        .email .icon i {
          margin-top: 30px;
          height: 50px;
          font-size: 1.25em;
          margin-left: 20px;
          display: block;
        }
        .phone {
          height: 50px;
          width: 270px;

          margin-top: 20px;
        }
        .phone .icon p {
          margin-top: 90px;
          display: inline;
          margin-left: 60px;
          font-size: 16px;
          color: #0000ee;
          font-weight: bold;
          line-height: 1.43;
        }
        .phone .icon i {
          margin-top: 30px;
          height: 50px;
          font-size: 1.25em;
          margin-left: 20px;
          display: block;
        }
        .company {
          height: 50px;
          width: 270px;
          margin-top: 10px;
        }
        .company .icon i p {
          margin-top: 80px;
          display: inline;
          margin-left: 20px;
          font-size: 13px;
          color: #00000099;
        }
        .company .icon i {
          margin-top: 20px;
          height: 50px;
          font-size: 1.25em;
        }
        .description {
          height: 50px;
          width: 270px;
          margin-top: 30px;
        }
        .description .icon i p {
          margin-top: 80px;
          display: inline;
          margin-left: 15px;
          font-size: 13px;
          color: #00000099;
        }
        .description .icon i {
          margin-top: 20px;
          height: 50px;
          font-size: 1.25em;
        }
        .status {
          height: 50px;
          width: 270px;
          margin-top: 30px;
        }
        .status .icon i p {
          margin-top: 80px;
          display: inline;
          margin-left: 15px;
          font-size: 13px;
          color: #00000099;
        }
        .status .icon i {
          margin-top: 20px;
          height: 50px;
          font-size: 1.25em;
        }
        .location {
          height: 50px;
          width: 270px;
          margin-top: -50px;

          margin-left: 300px;
        }
        .location .icon i p {
          margin-top: 80px;
          display: inline;
          margin-left: 20px;
          font-size: 13px;
          color: #00000099;
        }
        .location .icon i {
          margin-top: 20px;
          height: 50px;
          font-size: 1.25em;
        }
        .work {
          height: 50px;
          width: 270px;
          margin-top: -50px;

          margin-left: 300px;
        }
        .work .icon i p {
          margin-top: 80px;
          display: inline;
          margin-left: 20px;
          font-size: 13px;
          color: #00000099;
        }
        .work .icon i {
          margin-top: 20px;
          height: 50px;
          font-size: 1.25em;
        }
        .about p {
          font-size: 16px;
          font-weight: 400;
          font-family: Poppins;
          -webkit-font-smoothing: antialiased;
          color: #000000ee;
        }
        .contact p {
          font-size: 14px;
          font-weight: 400;
          font-family: Poppins;
          -webkit-font-smoothing: antialiased;
        }
        p {
          font-size: 12px;
          font-family: Normal;

          line-height: 1.167;

          color: rgba(0, 0, 0, 0.87);
        }
        .followers {
          width: 50px;
          height: 50px;
          position: absolute;
          top: 150px;
          left: 700px;
          padding-left: 24px;
          padding-right: 24px;
          padding-top: 5px;
          font-size: 18px;
          font-family: Normal;
          font-weight: bold;
          line-height: 1.167;
          color: #ffffff;
          text-align: center;
        }
        .followers p {
          color: rgba(255, 255, 255, 0.74);
          cursor: pointer;
          font-size: 12px;
          text-align: center;
        }
        .following p {
          color: rgba(255, 255, 255, 0.74);
          cursor: pointer;
          font-size: 12px;
          text-align: center;
        }
        .friends p {
          color: rgba(255, 255, 255, 0.74);
          cursor: pointer;
          font-size: 12px;
          text-align: center;
        }
        .following {
          width: 50px;
          height: 50px;
          position: absolute;
          top: 150px;
          left: 800px;
          padding-left: 24px;
          padding-right: 24px;
          padding-top: 5px;
          border-left: 1px solid white;
          font-size: 20px;
          font-family: Normal;
          font-weight: bold;
          line-height: 1.167;
          color: #ffffff;
          text-align: center;
        }
        .friends {
          width: 50px;
          height: 50px;
          position: absolute;
          top: 150px;
          left: 900px;
          padding-left: 24px;
          padding-right: 24px;
          padding-top: 5px;
          border-left: 1px solid white;
          font-size: 20px;
          font-family: Normal;
          font-weight: bold;
          line-height: 1.167;
          color: #ffffff;
          text-align: center;
        }
        .col-xs-7 {
          position: absolute;
          top: 100px;
          left: 200px;
        }
        .col-xs-7 h3 {
          font-size: 25px;
          font-family: Normal;
          font-weight: bold;
          line-height: 1.167;
          color: #ffffff;
        }
        .profile .img-circle {
          width: 130px;
          height: 130px;
          border-radius: 50%;
          border: 2px solid white;
        }

        .profile .grid-header .img-circle {
          margin: 2em 1em;
        }

        .profile .grid-header p {
          margin-bottom: 5px;
        }

        .profile .grid-header p a {
          color: #f5f5f5;
        }

        .profile .grid-body {
          font-size: 1em;
        }

        .profile .tab-content {
          padding-bottom: 0px;
        }

        .profile .stats {
          margin-top: 20px;
          padding: 0px 0px 20px;
          background: #eee;
          text-align: center;
          border-top: 1px solid #ccc;
        }

        .profile .stats h1 {
          font-weight: 400;
          margin-bottom: 0px;
        }

        .profile .stats span {
          margin: 5px 0 10px 0;
          display: block;
        }

        .profile .stats .btn {
          padding-right: 4em;
          padding-left: 4em;
        }

        .profile .timeline-centered {
          font-size: 1.15em;
        }

        .profile .timeline-centered:before {
          background: #eee;
        }

        .profile .bg-default {
          border: 1px solid #bbb;
        }

        .grid {
          position: relative;
          width: 100%;
          background: #fff;
          color: #666666;
          border-radius: 2px;
          margin-bottom: 25px;
          box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
        }

        .grid .grid-header {
          position: relative;
          border-bottom: 1px solid #ddd;
          padding: 15px 10px 10px 20px;
        }

        .grid .grid-header:before,
        .grid .grid-header:after {
          display: table;
          content: " ";
        }

        .grid .grid-header:after {
          clear: both;
        }

        .grid .grid-header span,
        .grid .grid-header > .fa {
          display: inline-block;
          margin: 0;
          font-weight: 300;
          font-size: 1.5em;
          float: left;
        }

        .grid .grid-header span {
          padding: 0 5px;
        }

        .grid .grid-header > .fa {
          padding: 5px 10px 0 0;
        }

        .grid .grid-header > .grid-tools {
          padding: 4px 10px;
        }

        .grid .grid-header > .grid-tools a {
          color: #999999;
          padding-left: 10px;
          cursor: pointer;
        }

        .grid .grid-header > .grid-tools a:hover {
          color: #666666;
        }

        .grid .grid-body {
          padding: 15px 20px 15px 20px;
          font-size: 0.9em;
          line-height: 1.9em;
        }

        .grid .full {
          padding: 0 !important;
        }

        .grid .transparent {
          box-shadow: none !important;
          margin: 0px !important;
          border-radius: 0px !important;
        }

        .grid.top.black > .grid-header {
          border-top-color: #000000 !important;
        }

        .grid.bottom.black > .grid-body {
          border-bottom-color: #000000 !important;
        }

        .grid.top.blue > .grid-header {
          border-top-color: #007be9 !important;
        }

        .grid.bottom.blue > .grid-body {
          border-bottom-color: #007be9 !important;
        }

        .grid.top.green > .grid-header {
          border-top-color: #00c273 !important;
        }

        .grid.bottom.green > .grid-body {
          border-bottom-color: #00c273 !important;
        }

        .grid.top.purple > .grid-header {
          border-top-color: #a700d3 !important;
        }

        .grid.bottom.purple > .grid-body {
          border-bottom-color: #a700d3 !important;
        }

        .grid.top.red > .grid-header {
          border-top-color: #dc1200 !important;
        }

        .grid.bottom.red > .grid-body {
          border-bottom-color: #dc1200 !important;
        }

        .grid.top.orange > .grid-header {
          border-top-color: #f46100 !important;
        }

        .grid.bottom.orange > .grid-body {
          border-bottom-color: #f46100 !important;
        }

        .grid.no-border > .grid-header {
          border-bottom: 0px !important;
        }

        .grid.top > .grid-header {
          border-top-width: 4px !important;
          border-top-style: solid !important;
        }

        .grid.bottom > .grid-body {
          border-bottom-width: 4px !important;
          border-bottom-style: solid !important;
        }
        span {
          margin-left: 40px;
          font-size: 16px;
          font-family: normal;
          color: #000000de;
          margin-bottom: 10px;
          diplay: block;
        }
      `}</style>
    </Layout>
  );
};
export const getServerSideProps = requirePageAuth;

export default Partnerinfos;
