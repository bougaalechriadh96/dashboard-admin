import { useEffect, useState } from "react";
import axios from "axios";
import Layout from "../components/layout";
import { Container, PageTitle, PageHeader } from "../components/widgets";
import { requirePageAuth } from "../utils/auth";
import Content from "../components/partners/Content";
import Filter from "../components/filter/Filter";
import { API_URL } from "../utils/consts";
const Index = ({ token }) => {
  const [partners, setPartners] = useState([]);
  const [loading, setLoading] = useState(false);
  const [filtered, setFiltered] = useState([]);
  const [activeGenre, setActiveGenre] = useState("");
  useEffect(() => {
    getAllPartners();
  }, [!loading]);

  const getAllPartners = () => {
    const config = {
      method: "GET",
      url: `${API_URL}/admins/partners`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    axios(config)
      .then(({ status, data }) => {
        if (status === 200) {
          setPartners(data.data);
          setFiltered(data.data);
        }
      })
      .catch((err) => {
        console.error("err", err);
      });
  };

  return (
    <Layout activeSection={1}>
      <PageTitle title="Partners" />
      <Filter
        partners={partners}
        setFiltered={setFiltered}
        activeGenre={activeGenre}
        setActiveGenre={setActiveGenre}
      ></Filter>
      <Container>
       
        <Content
          token={token}
          data={filtered}
          partners={partners}
          setLoading={setLoading}
          loading={loading}
        />
      </Container>
    </Layout>
  );
};

export const getServerSideProps = requirePageAuth;

export default Index;
