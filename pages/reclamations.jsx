import { useEffect, useState } from "react";
import axios from "axios";
import Layout from "../components/layout";
import { Container, PageTitle, PageHeader } from "../components/widgets";
import { requirePageAuth } from "../utils/auth";
import Reclamations from "../components/Reclamations/Reclamations";
import Content from "../components/Reclamations/Content";
import AddCategoryModal from "../components/categories/AddCategoryModel";
import { API_URL } from "../utils/consts";

const Index = ({ token }) => {
  const [reclamations, setReclamations] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isAddModalOpen, setIsAddModalOpen] = useState(false);
  const [reclamationId, setReclamationId] = useState("");

  useEffect(() => {
    getAllReclamations();
  }, [!loading]);

  const getAllReclamations = () => {
    const config = {
      method: "GET",
      url: `${API_URL}/admins/bookreclamations`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    axios(config)
      .then(({ status, data }) => {
        if (status === 200) {
          setReclamations(data.data);
        }
      })
      .catch((err) => {
        console.log("hello");
        console.error("err", err);
      });
  };

  const handleCloseAddModal = () => {
    setIsAddModalOpen(false);
  };

  return (
    <Layout activeSection={5}>
      <PageTitle title="Reclamations" />
      <Container>
        <Content
          setIsAddModalOpen={setIsAddModalOpen}
          token={token}
          data={reclamations}
          getAllReclamations={getAllReclamations}
          setLoading={setLoading}
          loading={loading}
          isModalOpen={isAddModalOpen}
          handleClose={handleCloseAddModal}
          reclamationId={reclamationId}
          setReclamationId={setReclamationId}
        />
      </Container>
      <Reclamations
        isModalOpen={isAddModalOpen}
        token={token}
        handleClose={handleCloseAddModal}
        reclamationId={reclamationId}
      />
    </Layout>
  );
};

export const getServerSideProps = requirePageAuth;

export default Index;
