import Content from "../components/dashboard/Content";
import Layout from "../components/layout";
import { Container, PageTitle } from "../components/widgets";
import { requirePageAuth } from "../utils/auth";

const Index = () => {
  return (
    <Layout activeSection={0}>
      <PageTitle title="Dashboard" />
      <Container>
        <Content />
      </Container>
    </Layout>
  );
};

export const getServerSideProps = requirePageAuth;

export default Index;
