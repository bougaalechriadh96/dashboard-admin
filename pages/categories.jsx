import { useEffect, useState } from "react";
import axios from "axios";
import Layout from "../components/layout";
import { Container, PageTitle, PageHeader } from "../components/widgets";
import { requirePageAuth } from "../utils/auth";
import Content from "../components/categories/Content";
import AddCategoryModal from "../components/categories/AddCategoryModel";
import { API_URL } from "../utils/consts";

const Index = ({ token }) => {
  const [categories, setCategories] = useState([]);
  const [isAddModalOpen, setIsAddModalOpen] = useState(false);

  useEffect(() => {
    getAllCategories();
  }, []);

  const getAllCategories = () => {
    const config = {
      method: "GET",
      url: `${API_URL}/categories`,
      // headers: {
      // 	Authorization: `Bearer ${token}`,
      // },
    };

    axios(config)
      .then(({ status, data }) => {
        if (status === 200) {
          setCategories(data.data);
          console.log(categories);
        }
      })
      .catch((err) => {
        console.log("hello");
        console.error("err", err);
      });
  };

  const handleCloseAddModal = () => {
    setIsAddModalOpen(false);
  };

  return (
    <Layout activeSection={2}>
      <PageTitle title="Categories" />
      <Container>
        <PageHeader
          type="category"
          handleAdd={() => {
            setIsAddModalOpen(true);
          }}
        />
        <Content token={token} data={categories} />
      </Container>

      <AddCategoryModal
        isModalOpen={isAddModalOpen}
        token={token}
        handleClose={handleCloseAddModal}
        getAllCategories={getAllCategories}
      />
    </Layout>
  );
};

export const getServerSideProps = requirePageAuth;

export default Index;
