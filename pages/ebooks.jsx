import { useEffect, useState } from "react";
import axios from "axios";
import Layout from "../components/layout";
import { Container, PageTitle, PageHeader } from "../components/widgets";
import { requirePageAuth } from "../utils/auth";
import Content from "../components/ebooks/Content";
import { API_URL } from "../utils/consts";
import Ebook from "../components/filter/Ebook";

const Index = ({ token }) => {
  const [ebooks, setEbooks] = useState([]);
  const [isAddModalOpen, setIsAddModalOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [filtered, setFiltered] = useState([]);
  useEffect(() => {
    getAllEBooks();
  }, [!loading]);

  const getAllEBooks = () => {
    const config = {
      method: "GET",
      url: `${API_URL}/ebooks`,
      // headers: {
      // 	Authorization: `Bearer ${token}`,
      // },
    };

    axios(config)
      .then(({ status, data }) => {
        if (status === 200) {
          setEbooks(data.data);
          setFiltered(data.data);
          console.log(ebooks);
        }
      })
      .catch((err) => {
        console.log("hello");
        console.error("err", err);
      });
  };

  const handleCloseAddModal = () => {
    setIsAddModalOpen(false);
  };

  return (
    <Layout activeSection={4}>
      <PageTitle title="EBooks" />
      <Ebook
        ebooks={ebooks}
        setFiltered={setFiltered}
        filtered={filtered}
      ></Ebook>
      <Container>
        <Content
          token={token}
          data={filtered}
          getAllEBooks={getAllEBooks}
          setLoading={setLoading}
          loading={loading}
        />
      </Container>
    </Layout>
  );
};

export const getServerSideProps = requirePageAuth;

export default Index;
