
// Get input time from date
export const dateToInputTime = date => {
    if (date) {
        let _date = new Date(date);
        _date = new Date(_date.getTime() - (_date.getTimezoneOffset() * 60000));

        return new Date(_date).toISOString().substring(11, 16);
    }

    return;
}

// Get input date from date
export const dateToInputDate = date => {
    if (date) {
        return new Date(date).toISOString().substring(0, 10);
    }

    return;
}